#!/bin/bash

# Generating translations files using textos-sueltos-unobike.xlsx
# Angular xliff
cd /home/alexis/unobike-elements && 
    ng i18n-extract --output-path src/locale --ivy && 
    /usr/bin/php /home/alexis/Escritorio/generate-xlif/angular-locales.php;

# PHP poe files
cd /var/www/unobike-web/public &&
    touch messages.pot &&
    find . ../src/Data/* -iname "*.php" | xargs xgettext --from-code=UTF-8 --width=5000 --add-comments messages.pot &&
    mv messages.po ../locale/es/LC_MESSAGES/messages.po &&
    rm messages.pot &&
    /usr/bin/php /home/alexis/Escritorio/generate-xlif/generate-po.php &&
    msgfmt ../locale/es/LC_MESSAGES/messages.po --output-file=../locale/es/LC_MESSAGES/messages.mo &&
    msgfmt ../locale/fr/LC_MESSAGES/messages.po --output-file=../locale/fr/LC_MESSAGES/messages.mo &&
    msgfmt ../locale/en/LC_MESSAGES/messages.po --output-file=../locale/en/LC_MESSAGES/messages.mo;

# Symfony xliff files
cd /var/www/unobike-web/ &&
    bin/console translation:update es --force &&
    bin/console translation:update en --force &&
    bin/console translation:update fr --force &&
    cd /home/alexis/Escritorio/generate-xlif/ &&
    /usr/bin/php /home/alexis/Escritorio/generate-xlif/generate-symfony-xlif.php;

echo "";
echo "Don't forget commit!!!";