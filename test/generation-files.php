<?php

/**
 * Run Command
 * 
 * /usr/bin/php /home/alexis/Escritorio/generate-xlif/test/generation-files.php
 * 
 */

require __DIR__ . '/../bootstrap.php';

// Custom ENV Variables
$_ENV['TRANSLATIONS_FILE'] = $_ENV['TEST_TRANSLATIONS_FILE'];

// Test Code
$baseTestData = '/home/alexis/Escritorio/generate-xlif/test/data';
$filesToCheck = [
    'test_messages.fr.xlf',
    'test_messages.en.xlf',
    'test_messages.es.po',
    'test_messages.fr.po',
    'test_messages.en.po',
    'test_messages+intl-icu.es.xlf',
    'test_messages+intl-icu.fr.xlf',
    'test_messages+intl-icu.en.xlf',
];

echo "\n--------------------------------------------\n";
echo "| TRANSLATIONS TEST\n";
echo "--------------------------------------------\n";
sleep(1);

// Run angular translation
echo "\n--------------------------------------------\n";
echo "| Generating angular files\n";
echo "--------------------------------------------\n";
@unlink('/home/alexis/Escritorio/generate-xlif/angular/messages.xlf');
@unlink('/home/alexis/Escritorio/generate-xlif/test/data/messages.fr.xlf');
@unlink('/home/alexis/Escritorio/generate-xlif/test/data/messages.en.xlf');
copy('/home/alexis/Escritorio/generate-xlif/test/data/source-messages.xlf', '/home/alexis/Escritorio/generate-xlif/angular/messages.xlf');

generateAngularTranslations();

copy('/home/alexis/Escritorio/generate-xlif/angular/messages.fr.xlf', $baseTestData . '/messages.fr.xlf');
copy('/home/alexis/Escritorio/generate-xlif/angular/messages.en.xlf', $baseTestData . '/messages.en.xlf');
@unlink('/home/alexis/Escritorio/generate-xlif/angular/messages.xlf');
@unlink('/home/alexis/Escritorio/generate-xlif/angular/messages.fr.xlf');
@unlink('/home/alexis/Escritorio/generate-xlif/angular/messages.en.xlf');

// Run Poe translations
@unlink('/home/alexis/Escritorio/generate-xlif/gettext/messages.po');
@unlink('/home/alexis/Escritorio/generate-xlif/test/data/messages.es.po');
@unlink('/home/alexis/Escritorio/generate-xlif/test/data/messages.fr.po');
@unlink('/home/alexis/Escritorio/generate-xlif/test/data/messages.en.po');
copy('/home/alexis/Escritorio/generate-xlif/test/data/source-messages.po', '/home/alexis/Escritorio/generate-xlif/gettext/messages.po');

generatePoeTranslations();

copy('/home/alexis/Escritorio/generate-xlif/gettext/messages.es.po', '/home/alexis/Escritorio/generate-xlif/test/data/messages.es.po');
copy('/home/alexis/Escritorio/generate-xlif/gettext/messages.fr.po', '/home/alexis/Escritorio/generate-xlif/test/data/messages.fr.po');
copy('/home/alexis/Escritorio/generate-xlif/gettext/messages.en.po', '/home/alexis/Escritorio/generate-xlif/test/data/messages.en.po');
@unlink('/home/alexis/Escritorio/generate-xlif/gettext/messages.po');
@unlink('/home/alexis/Escritorio/generate-xlif/gettext/messages.es.po');
@unlink('/home/alexis/Escritorio/generate-xlif/gettext/messages.fr.po');
@unlink('/home/alexis/Escritorio/generate-xlif/gettext/messages.en.po');

// Run Symfony translations
$files = [
    '/home/alexis/Escritorio/generate-xlif/test/data/source-messages+intl-icu.es.xlf',
    '/home/alexis/Escritorio/generate-xlif/test/data/source-messages+intl-icu.fr.xlf',
    '/home/alexis/Escritorio/generate-xlif/test/data/source-messages+intl-icu.en.xlf',
];

generateSymfonyTranslations($files);

copy('/home/alexis/Escritorio/generate-xlif/symfony/messages+intl-icu.es.xlf', '/home/alexis/Escritorio/generate-xlif/test/data/messages+intl-icu.es.xlf');
copy('/home/alexis/Escritorio/generate-xlif/symfony/messages+intl-icu.fr.xlf', '/home/alexis/Escritorio/generate-xlif/test/data/messages+intl-icu.fr.xlf');
copy('/home/alexis/Escritorio/generate-xlif/symfony/messages+intl-icu.en.xlf', '/home/alexis/Escritorio/generate-xlif/test/data/messages+intl-icu.en.xlf');
@unlink('/home/alexis/Escritorio/generate-xlif/symfony/messages+intl-icu.es.xlf');
@unlink('/home/alexis/Escritorio/generate-xlif/symfony/messages+intl-icu.fr.xlf');
@unlink('/home/alexis/Escritorio/generate-xlif/symfony/messages+intl-icu.en.xlf');

// Check files
echo "\n--------------------------------------------\n";
echo "| Comparing files\n";
echo "--------------------------------------------\n";
foreach($filesToCheck as $file) {

    $fileNameGenerated  = str_replace('test_', '', $file);
    $fileNameTest       = $file;

    $fileGenerated  = $baseTestData . '/' . $fileNameGenerated;
    $fileTest       = $baseTestData . '/' . $fileNameTest;

    $fileGeneratedContent   = '';
    $fileTestContent        = '';

    // Read test file    
    $fp = fopen($fileGenerated, 'r');
    $fileGeneratedContent = fread($fp, filesize($fileGenerated));    
    fclose($fp);

    $fp = fopen($fileTest, 'r');
    $fileTestContent = fread($fp, filesize($fileTest));    
    fclose($fp);

    if ( $fileGeneratedContent == $fileTestContent ) {
        echo "- Files match $fileNameTest $fileNameGenerated \n";
    } else {
        throw new Exception("Error: Files not match $fileNameTest $fileNameGenerated");
    }
}