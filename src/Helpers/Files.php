<?php

namespace App\Helpers;

class Files {

    public static function getAngularMessagesXlf()
    {
        copy( $_ENV['SOURCE_ANGULAR_FILES'] . '/messages.xlf', '/home/alexis/Escritorio/generate-xlif/angular/messages.xlf');    
    }
    
    public static function moveGeneratedAngularFiles()
    {    
        copy('/home/alexis/Escritorio/generate-xlif/angular/messages.fr.xlf', $_ENV['SOURCE_ANGULAR_FILES'] . '/messages.fr.xlf');
        copy('/home/alexis/Escritorio/generate-xlif/angular/messages.en.xlf', $_ENV['SOURCE_ANGULAR_FILES'] . '/messages.en.xlf');    
    }
}