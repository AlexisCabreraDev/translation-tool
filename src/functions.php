<?php

use PhpOffice\PhpSpreadsheet\IOFactory;

// Angular functions
function loadTranslations( string $fileTextosSueltos ): array
{
    $locales = explode(' ', $_ENV['LOCALES_AVAILABLES']);
    $localPath = ($_ENV['PATH_LOCAL_TRANSLATIONS_FILE'][0] == '/')
        ? $_ENV['PATH_LOCAL_TRANSLATIONS_FILE']
        : __DIR__ . '/../../' . $_ENV['PATH_LOCAL_TRANSLATIONS_FILE'];

    $data = file_exists( $localPath )
        ? file_get_contents( $localPath )
        : file_get_contents( $fileTextosSueltos );

    $rows = explode("\n",$data);
    $s = [];
    $translationGroup = [];
    $firstRow = true;
    $line = 1;

    foreach($rows as $row) {

        if ( $firstRow ) {
            $firstRow = false;
        } else {
            $s = str_getcsv($row, ';');

            $line++;
            $translationGroup[$s[0]]['es'] = $s[0];

            $index = 1;
            foreach($locales as $locale) {
              $translationGroup[$s[0]][$locale] = $s[$index];
              $index++;
            }

            $translationGroup[$s[0]]['like'] = $line;
        }
    }

    return $translationGroup;
}

function translate( string $toTranslate, string $locale, array $translations ): string
{
    if ( isset($translations[$toTranslate]) ) {

        $translateCandidate = $translations[$toTranslate][$locale];

        if ( strlen($translateCandidate) ) {
            return $translateCandidate;
        }
    }

    return '__' . trim($toTranslate);
}

// Poe functions
function transpileLocale( string $locale, array $translations)
{
    $filePoEs = '/home/alexis/Escritorio/generate-xlif/gettext/messages.po';
    $fileTargetBase = "/home/alexis/Escritorio/generate-xlif/gettext/";
    $fileTarget = $fileTargetBase . "messages.$locale.po";
    $i = 0;
    $localesLabel = ['es' => 'es_ES', 'fr' => 'fr_FR', 'en' => 'en_EN'];
    $newPoFile = '';

    $handle = fopen($filePoEs, "r");
    while (($line = fgets($handle)) !== false) {

        $i++;

        if ( $line == '"Language: \n"' ) {
            $line = '"Language: ' . $localesLabel[$locale] . '\n"';
        }

        if ( $i < 18 ) {
            $newPoFile .= $line;
        } else {
            $newPoFile .= makeTranslation($line, $locale, $translations);
        }
    }

    $fp = fopen($fileTarget, 'w');
    fwrite($fp, $newPoFile);
    fclose($fp);
}

function generatePoeTranslations()
{
    $locales = ['es', 'fr', 'en'];
    $translations = loadTranslations($_ENV['TRANSLATIONS_FILE']);

    foreach( $locales as $locale ) {
        echo "Transpilando poe $locale\n";
        transpileLocale($locale, $translations);
    }
}

function mainGeneratePoeTranslations()
{
    checkIntegrity('/var/www/unobike-web/locale/es/LC_MESSAGES/messages.po');

    copy('/var/www/unobike-web/locale/es/LC_MESSAGES/messages.po', '/home/alexis/Escritorio/generate-xlif/gettext/messages.po');

    generatePoeTranslations();

    copy('/home/alexis/Escritorio/generate-xlif/gettext/messages.es.po', '/var/www/unobike-web/locale/es/LC_MESSAGES/messages.po');
    copy('/home/alexis/Escritorio/generate-xlif/gettext/messages.fr.po', '/var/www/unobike-web/locale/fr/LC_MESSAGES/messages.po');
    copy('/home/alexis/Escritorio/generate-xlif/gettext/messages.en.po', '/var/www/unobike-web/locale/en/LC_MESSAGES/messages.po');
}

// Symfony functions
function makeTranslation( string $line, string $locale, array $translations): string
{
    $isMsgid = preg_match('/^msgid /', $line);
    $isMsgstr = preg_match('/^msgstr /', $line);

    if ( $isMsgstr ) {
        return '';
    }

    if ( !$isMsgid ) {
        $lineHead = substr($line, 0, 3);

        if ( $lineHead == '#: ') {
            $lineTmp = str_replace('#: ', '', $line);
            $lineParts = explode(' ', $lineTmp);

            if ( count($lineParts) > 1 ) {
                $line = "#: " . implode("\n#: ", $lineParts);
            }
        }

        return $line;
    }

    preg_match('/"(.*)"/', $line, $match);
    $textToTranslate = trim($match[1]);

    if ( $textToTranslate == '""' ) {
        return 'msgid ""'."\n" . 'msgstr ""'."\n";
    }

    if ( empty($translations[$textToTranslate])) {
        return $line . 'msgstr "__' . $textToTranslate . '"'."\n" . "\n";
    }

    switch($locale) {
        case 'fr':
            return $line . 'msgstr "' . $translations[$textToTranslate] . '"' . "\n" . "\n";
        case 'en':
            return $line . 'msgstr "__' . $textToTranslate . '"'."\n" . "\n";
        default:
            return $line . 'msgstr "' . $textToTranslate . '"'."\n" . "\n";
    }
}

function checkIntegrity( string $filePoEs ) {

    $text = file_get_contents($filePoEs);

    $found = preg_match('/msgid ".*"\s+".*"/', $text, $match);

    if ( $found ) {
        print_r($match);
        throw new Exception('Translation have several lines');
    }

    if ( strpos($text, '<p>') !== false ) {
        throw new Exception('Invalid html detected');
    }
}

function getAngularMessagesXlf()
{
    copy( __DIR__ . '/../../src/locale/messages.xlf', __DIR__ . '/../angular/messages.xlf');
}

function moveGeneratedAngularFiles()
{
    $locales = explode(' ', $_ENV['LOCALES_AVAILABLES']);

    foreach($locales as $locale) {
        copy(__DIR__ . '/../angular/messages.'.$locale.'.xlf', __DIR__ . '/../../src/locale/messages.'.$locale.'.xlf');
    }
}

function generateAngularTranslations()
{
    $file = __DIR__ . "/../angular/messages.xlf";
    $fileTargetBase = __DIR__ . "/../angular/";
    $locales = explode(' ', $_ENV['LOCALES_AVAILABLES']);
    $translationTarget = '';
    $translations = loadTranslations($_ENV['TRANSLATIONS_FILE']);

    foreach( $locales as $locale ) {

        $fileTarget = $fileTargetBase . "messages.$locale.xlf";
        $translationTarget = '';

        $i = 0;
        $handle = fopen($file, "r");
        while (($line = fgets($handle)) !== false) {

            $i++;

            if (
                strpos($line, '<?xml') !== false
                || strpos($line, '<xliff') !== false
                || strpos($line, '</xliff') !== false
                || strpos($line, '<file') !== false
                || strpos($line, '</file') !== false
                || strpos($line, '<body') !== false
                || strpos($line, '</body') !== false
                || strpos($line, '<trans-unit') !== false
                || strpos($line, '</trans-unit') !== false
                || strpos($line, '<source') !== false
            ) {

                if ( strpos($line, '<file') !== false ) {
                    $translationTarget .= '<file source-language="es" target-language="' . $locale . '" datatype="plaintext" original="ng2.template">' . "\n";
                    continue;
                }

                if ( strpos($line, '<source') !== false ) {
                    preg_match('/<source>([^<]+)/', $line, $match);

                    if ( isset($match[1]) ) {

                        $translationTarget .= $line;

                        $translationTarget .= "\t\t\t\t<target>"
                            . translate($match[1], $locale, $translations) .
                        "</target>\n";

                        continue;
                    } else {
                        echo 'source not found'; exit;
                    }
                }

                $translationTarget .= $line;

            } else {
                if (
                    strpos($line, '<context') !==  false
                    || strpos($line, '</context') !==  false
                ) {
                    // ignore
                } else {
                    echo $i . '. ' . $line;
                    echo 'unkown line';
                    exit;
                }
            }
        }
        fclose($handle);

        echo "processed\n";

        $fp = fopen($fileTarget, 'w');
        fwrite($fp, $translationTarget);
        fclose($fp);

        echo "Translated $locale\n";
    }
}

function mainGenerateAngularTranslations()
{
    // First copys original xlif
    @unlink(__DIR__ . '/../angular/messages.xlf');

    getAngularMessagesXlf();

    generateAngularTranslations();

    // Replace other language files
    moveGeneratedAngularFiles();

    echo 'Translations done COMMIT AND ENJOY!!!';
}

// Symfony functions
function processTranslationFile( string $file, array $translations )
{
    echo "Procesando $file \n";
    $fileTargetBase = __DIR__ . '/../symfony/';
    $translationContent = '';
    preg_match('/\.([a-z]+)\./', $file, $matchLocale);
    $locale = $matchLocale[1];
    $fileTarget = $fileTargetBase . 'messages+intl-icu.' . $locale . '.xlf';

    $handle = fopen($file, "r");
    while (($line = fgets($handle)) !== false) {

        if ( strpos($line, '<target>') !== false ) {

            preg_match('/<target>__([^<]+)/', $line, $match);

            if ( isset($match[1]) ) {

                $keyTranslation = $match[1];

                if ( isset($translations[$keyTranslation]) ) {

                    $tranlationCandidate = $translations[$keyTranslation][$locale];

                    if ( strlen($tranlationCandidate) > 0 ) {
                        $line = str_replace($match[0], '<target>' . $tranlationCandidate, $line);
                    }
                }
            }
        }

        $translationContent .= $line;
    }

    $fp = fopen($fileTarget, 'w');
    fwrite($fp, $translationContent);
    fclose($fp);
}

function generateSymfonyTranslations( array $files )
{
    $translations = loadTranslations($_ENV['TRANSLATIONS_FILE']);

    foreach ( $files as $file ) {
        processTranslationFile( $file, $translations );
    }
}

function mainGenerateSymfonyTranslations()
{
    $locales = explode(' ', $_ENV['LOCALES_AVAILABLES']);
    $files = [];

    $files[] = __DIR__ . '/../../translations/messages+intl-icu.es.xlf';

    foreach( $locales as $locale ) {
        $files[] = __DIR__ . '/../../translations/messages+intl-icu.'.$locale.'.xlf';
    }

    generateSymfonyTranslations($files);

    copy( __DIR__ . '/../symfony/messages+intl-icu.es.xlf', __DIR__ . '/../../translations/messages+intl-icu.es.xlf');

    foreach( $locales as $locale ) {
        copy( __DIR__ . '/../symfony/messages+intl-icu.'.$locale.'.xlf', __DIR__ . '/../../translations/messages+intl-icu.'.$locale.'.xlf');
    }

    echo 'Translations done COMMIT AND ENJOY!!!';
}
