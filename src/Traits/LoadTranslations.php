<?php

namespace App\Traits;

use PhpOffice\PhpSpreadsheet\IOFactory;

trait LoadTranslations {

  abstract public function getLocales(): array;

  public function getTranslations( string $fileTextosSueltos ): array
  {
    $columns = range('A', 'Z');
    $headers = [];
    $localesList = $this->getLocales();
    $translationMap = [];

    $nLocales = count($localesList);    
    $firstColumn = $columns[0];
    $lastColumn = $columns[$nLocales];

    $spreadsheet = IOFactory::load($fileTextosSueltos);
    $sheet = $spreadsheet->getActiveSheet();
    $highestRow = $sheet->getHighestRow();

    $headers = $sheet->rangeToArray($firstColumn . '1:' . $lastColumn . '1', null, true, true)[0];
    $headers = array_map(function($item) {
      return strtolower($item);
    }, $headers);

    for ( $i = 2; $i <= $highestRow; $i++ ) {

        $row = $sheet->rangeToArray($firstColumn . $i . ':' . $lastColumn . $i, null, true, true)[0];

        for ( $j = 1; $j <= $nLocales; $j++ ) {
          $key = $row[0];
          $keyLocale = $headers[$j];
          $translationMap[$row[0]][$keyLocale] = $row[$j];
        }
    }

    return $translationMap;
  }

}