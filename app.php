<?php

require __DIR__ . '/bootstrap.php';

use App\Traits\LoadTranslations;

class Primera {

  const LOCALE_LIST = ['es', 'fr', 'en', 'pt'];

  use LoadTranslations;

  public function getLocales(): array
  {
    return self::LOCALE_LIST;
  }

}

print_r( (new Primera())->getTranslations('/home/alexei/translation-tool/data/textos-sueltos-unobike.xlsx') );